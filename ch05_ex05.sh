#!/bin/bash
# Chapter 5, exercise 5. The script should delete the first 3 lines of each temporary file.
# Print to standard output the lines containing the 'an' pattern.

LINE2ADD="*** This might have something to do with man and man pages ***"
ls -Al /usr/bin/?a* > /var/tmp/file_list 2>&1
echo "Trying to remove the first 3 lines from the .tmp files..."
echo
sed -i -n '4,$p' *.tmp 2>&1
if ((`echo $?`!=0)); then
    echo "There are no .tmp temporary files to remove the first 3 lines from."
    echo
else
    echo "Lines successfully removed."
fi
echo "These are the lines containing the 'an' string:"
sed -n '/an/p' /var/tmp/file_list 2>&1 | sed "/man/i $LINE2ADD"
echo
