#!/bin/bash
# Chapter 4 exercise 11, check whether an user exists in /etc/passwd.

if (($#==0)); then
    echo Please enter an user name to be checked:
    read UName
    grep $UName /etc/passwd > /dev/null 2>&1
        if ((`echo $?`==0)); then
            echo The user $UName exists.
            echo This is the info from the passwd file:
            echo
            grep $UName /etc/passwd
            echo
        else
            echo The user $Uname does not exist.
        fi
else
    UName=$1
    echo I will now check if user $UName has an account on this system:
    grep $UName /etc/passwd > /dev/null 2>&1
        if ((`echo $?`==0)); then
            echo The user $UName exists.
            echo This is the info from the passwd file:
            echo
            grep $UName /etc/passwd
            echo
        else
            echo The user $Uname does not exist.

        fi
fi
