#!/bin/bash
# Script created for exercise 8 from Chapter 4.
# The script should make a list of files in the HOME directory that were changed less than 10 hours ago.

find $HOME -maxdepth 1 -type f -mmin -600

