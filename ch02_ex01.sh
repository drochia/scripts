#!/bin/bash
# This is the script created for exercise 1 from Chapter 2.
# It will display the path to my homedirectory, terminal type I am using 
# and services started up in runlevel 3 on my system.

clear

echo "This is my home directory: $HOME."		# display the path to my home directory
echo

echo "This is the terminal that I am using: $TERM."	# shows the terminal that I am using 
echo

printf "Those are the services started up in runlevel 3 on my system:\n"	# Services started at runlevel 3
ls -lF /etc/rc3.d/S*
echo

echo "This is the end of the script.  I hope there are no errors in it."
