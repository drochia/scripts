syntax on		" highlight syntax
set background=dark	" set the background to dark
set number		" show line numbers
set noswapfile		" disable the swapfile
set hlsearch		" highlight all results
set ignorecase		" ignore case in search
set incsearch		" show search results as you type
let mapleader = "'"
inoremap jk <ESC>	" map jk to act as <ESC>
