#!/bin/bash

# Script created for exercise 9 from chapter 3.  The script sould calculate the surface of a rectangle
# from the value it receives.

EDGE1=$1
EDGE2=$2
SURFACE=$((EDGE1 * EDGE2))

echo "The rectangle surface equals to $SURFACE"
