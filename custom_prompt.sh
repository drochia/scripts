#!/bin/bash
# custom shell script, should be added to /etc/profile.d folder
# should be sourced in: /etc/bash.bashrc

RED="\[\033[1;31m\]"
GREEN="\[\033[1;32m\]"
WHITE="\[\033[0;37m\]"
DEFAULT="\[\033[0m\]"
CYAN="\[\033[0;36m\]"

if (( $UID == 0 )); then
    Prompt_my="$RED\u@\h$WHITE:$CYAN\w$WHITE#$DEFAULT "
else
    Prompt_my="$GREEN\u@\h$WHITE:$CYAN\w$WHITE\$$DEFAULT "
fi

export PROMPT_COMMAND='export PS1="$Prompt_my"'

# export PROMPT_COMMAND='export PS1="\[\033[0;32m\]\u@\h\[\033[0;37m\]:\[\033[0;36m\]\w\[\033[0;37m\]$\[\033[0m\] "'
