#!/bin/bash
# Chapter 5, exercise 6. In the long listing of a directory using 'sed' the symbolic link files should be preceded
# with a line containing a message and the plain file should be added a comment at the end of line.

LINKCOMMENT="--This is a symlink--"
FILECOMMENT="   <---this is a plain file"

if (($#==0)); then
    echo "Please enter the PATH to a directory to work on:"
    read WORKINGFOLDER
    ls $WORKINGFOLDER > /dev/null 2>&1
        if ((`echo $?`==0)); then
            echo The directory $WORKINGFOLDER exists and this is the content:
            ls -Al $WORKINGFOLDER | sed -e "/^-/s/$/$FILECOMMENT/" | sed -e "/^l/i $LINKCOMMENT"
            echo
        else
            echo The directory $WORKINGFOLDER does not exist.
        fi
else
    WORKINGFOLDER=$1
    echo I will now check if the directory $WORKINGFOLDER exists:
    ls $WORKINGFOLDER > /dev/null 2>&1
        if ((`echo $?`==0)); then
            echo The directory $WORKINGFOLDER exists and this is the content:
            ls -Aal $WORKINGFOLDER | sed -e "/^-/s/$/$FILECOMMENT/" | sed -e "/^l/i $LINKCOMMENT"
            echo
        else
            echo The directory $WORKINGFOLDER does not exist.
        fi
fi
echo
